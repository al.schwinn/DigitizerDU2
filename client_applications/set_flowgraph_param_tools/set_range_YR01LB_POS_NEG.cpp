//RDA includes
#include <cmw-rda3/client/service/ClientServiceBuilder.h>
#include <cmw-rda3/common/config/Defs.h>

//misc includdes
#include <iostream>
#include <string>
#include <stdlib.h>     /* getenv */


using namespace std;
using namespace cmw::util;
using namespace cmw::data;
using namespace cmw::rda3::client;
using namespace cmw::rda3::common;


void printException(const RdaException& ex)
{
    std::cout << "Exception caught:" << std::endl;
    cout << ex.what() << endl;
    std::vector<std::string> stack = ex.getStack();
    std::vector<std::string>::iterator iter;
    for ( iter = stack.begin(); iter != stack.end(); iter++ )
    {
        std::cout << *iter << std::endl;
    }
}

void help ()
{
    cout << "" << endl;
    cout << "\tUsage: set_range_YR01LB_POS_NEG <range>" << endl;
    cout << "\tThis script will set the specified Range value for the Digitizer Channels YR01LB:U_POS:Triggered@Raw and YR01LB:U_NEG:Triggered@Raw" << endl;
    cout << "" << endl;
    cout << "\tPossible values for 'range': 0.5, 1, 2, 5, 10" << endl;
    cout << "" << endl;
}

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        cout << "Error: Missing argument"<< endl;
        help();
        return 1;
    }

    if (argc > 2)
    {
        cout << "Error: To many argument"<< endl;
        help();
        return 1;
    }

    std::string arg(argv[1]);

    if (arg == "--help" || arg == "-h")
    {
        help();
        return 0;
    }

    try
    {
        float range = std::stof(arg);

        if (range != 0.5 &&
            range != 1. &&
            range != 2. &&
            range != 5. &&
            range != 10.)
        {
            cout << "Error: The specified Range is not supported" << endl;
            help();
            return 1;
        }
        std::vector<std::string> paramValues;
        paramValues.push_back(string("digitizers_picoscope_3000a_0|range_ai_b|") + arg);
        paramValues.push_back(string("digitizers_picoscope_3000a_0|offset_ai_b|0"));
        paramValues.push_back(string("digitizers_block_scaling_offset_channel_B|offset|0"));
        paramValues.push_back(string("digitizers_picoscope_3000a_0|range_ai_c|") + arg);
        paramValues.push_back(string("digitizers_picoscope_3000a_0|offset_ai_c|0"));
        paramValues.push_back(string("digitizers_block_scaling_offset_channel_C|offset|0"));

        // For testing
//        setenv ("CMW_DIRECTORY_CLIENT_SERVERLIST","cmwdev00a.acc.gsi.de:5021",true);
//        std::string server_name = "DigitizerDU2.dal004";
//        std::string device_name = "FG2";

        setenv ("CMW_DIRECTORY_CLIENT_SERVERLIST","cmwpro00a.acc.gsi.de:5021",true);
        std::string server_name = "DigitizerDU2.dal024";
        std::string device_name = "YRCD001";

        std::string cycle_name = "";
        std::string prop_name = "FlowGraphParameter";

        std::cout << "---------------------------------------------------------------"<< std::endl;
        std::cout << "Setting Range of POS/NEG channel to" << range << " for Server: " << server_name << " Device: "<< device_name << std::endl;
        std::cout << "---------------------------------------------------------------"<< std::endl;

        auto clientService = ClientServiceBuilder::newInstance()->build();
        AccessPoint* accessPoint;
        accessPoint = &(clientService->getAccessPoint(device_name, "FlowGraphParameter" ,server_name));

        // It is required to set an emptyfilter to make Fesa happy
        auto filters = DataFactory::createData();
        const char *filter_strings[1];
        filter_strings[0] = "";
        filters->appendArray("parameterFilters",filter_strings,1);
        auto context = RequestContextFactory::create(cycle_name, filters);

        auto data = DataFactory::createData();
        const char *data_strings[paramValues.size()];
        for(unsigned int i=0;i<paramValues.size();i++)
          data_strings[i] = paramValues[i].c_str();

        data->appendArray("parameterValues",data_strings,paramValues.size());
        accessPoint->set(data, context);

        std::cout << "Range set sucesfully ... please be patient now, the Digitizer class is restarting" << std::endl;
    }
    catch (const RdaException& ex)
    {
        printException(ex);
        return 1;
    }
    catch (const std::exception& ex)
    {
        cout << "Exception caught: " << ex.what() << endl;
    }
    return 0;
}
