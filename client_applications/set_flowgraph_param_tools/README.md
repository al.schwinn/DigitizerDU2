# set_range_YR01LB_POS_NEG

RDA Client in order to set the range for the Digitizer Channels YR01LB:U_POS:Triggered@Raw and YR01LB:U_NEG:Triggered@Raw

# Build and Run

```
make clean
make
./set_range_YR01LB_POS_NEG
```
