/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerDU2/RealTime/RTEquipment.h>
#include <fesa-core/Core/AbstractServiceLocator.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
namespace DigitizerDU2
{

RTEquipment::RTEquipment(const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol) :
	RTEquipmentGen(serviceLocatorCol)
{
}

RTEquipment::~RTEquipment()
{
}

void RTEquipment::specificInit()
{
}

void RTEquipment::handleEventSourceError (fesa::AbstractEventSource* eventSource,fesa::FesaException& exception)
{
}
void RTEquipment::handleSchedulerError (fesa::RTScheduler* scheduler,fesa::FesaException& exception)
{
}
}
