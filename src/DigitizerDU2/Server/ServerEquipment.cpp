/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerDU2/Server/ServerEquipment.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
namespace DigitizerDU2
{

ServerEquipment::ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol) : ServerEquipmentGen(serviceLocatorCol)
{
}

ServerEquipment::~ServerEquipment ()
{
}

void ServerEquipment:: specificInit () 
{
}

}
