/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef DigitizerDU2_SERVER_EQUIPMENT_H_
#define DigitizerDU2_SERVER_EQUIPMENT_H_

#include <fesa-core/Server/AbstractServerEquipment.h>

#include <fesa-core/Exception/FesaException.h>

#include <DigitizerDU2/GeneratedCode/ServerEquipmentGen.h>

namespace DigitizerDU2
{

class ServerEquipment : public ServerEquipmentGen
{
	public:

		ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol);

		virtual  ~ServerEquipment();

		void specificInit();

};
}
#endif
