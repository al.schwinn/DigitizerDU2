
# Before running tests

Most of the test located in this folder require events to be injected. To do that run `Inject_simulation.sh`
script located in the `inject` folder.

Then whenewer a given folowgraph is restarted, make sure to configure timing events by using Setting property:
 - 123 -> TIME_SYNC
 - 35  -> BEAM_IN
 - 280 -> USED
 - ...
 
Note most examples will work without those event configured but the acqTriggerName in the Acquisition/Spectra
proeprty would in this case read UNKNOWN.

# Common HW configuration

The following HW connections are assumed:

A - Input signal A
B - Input signal B
C - Trigger (connected to the timing card)
D - Kicker Trigger (for ralignment delay detection)

# A.1 Digitizer

In order to test configuratio supply, select any of the GR flowgraphs that use a digitizer as a source. Connect
a signal generator, change settings of connected channel so that a signal is cut off by a small margin. Change
configuration from the FESA explorer so that the signal is not cut off anymore. E.g. range can be changed from +-2V
to +-5V or similar.

Note in order to apply configuration an appropriate SEQ_START event needs to be generated. This can be tested
even with the simulated timing:

```shell
[localhost]$ ./startManually_DigitizerDU_M.sh -f -timsim ./localhostTiming.xml

```
Also if debug level is set to TRACE, new configuration will be printed out. This new configuration will be
applied only if differs from the previous configuration.

# B.1 Scaling & Offset

GRC file: grc/b1_scaling_offset.grc

No HW is needed. Just run the graph and play around with gain and offset settings.

# B.2 Time Realignment and Demux

GRC file: grc/fesa_edge_detect_tc.grc

Assumed HW configuration:

A - Trigger (connected to the timing card)
B - Kicker Trigger (for ralignment delay detection)

By using ths flowgraph, both triggered and snapshot acquisition modes can be tested. Please make sure to configure
timing events properly.

# B.3 Data Aggregation & Decimation

GRC file: grc/b3_data_aggregation.grc

Assumed HW configuration:

A - Some kind of signal for exercising different filters.

# B.4 STFT (Static)

GRC file: fesa_freq_domain.grc

No HW is required. Change upper and lower frequency and observe spectra.

# B.5 Amplitude and Phase detection


# B.6 Chi Square

GRC file: grc/b6_chi_square.grc

No HW required. Kind of difficult to test all the parameters. I used debugger to make sure that the parameters
are properly passed in.

# B.7 Interlock detection

GRC file: grc/fesa_interlock.grc

Use common HW configuration. Update reference function if needed. Change input signal A so that it triggers an
interlock. Clear the interlock by using the UnlatchInterlock property.

Don't forget to apply timing configuraiton by using the Setting property.

# FAT

HW configuration as explained in first two sections. Use 1kHz source for channels A and B for starters.
