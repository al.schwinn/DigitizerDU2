#!/bin/sh
. /etc/functions

# For the Digitzers there is a seg.fault when boost-filesystem is used (when the daemon is started with -conf-dir)
# To fix that, we start the deamon in the same way than we start the binary manually: Via the "startManually" script

# load dhcp options
[ -f /etc/defaults/dhcp-options ] && . /etc/defaults/dhcp-options

FESA_BASE=/opt/fesa
FESA_MOUNT="$FESA_BASE/nfs"
FESA_SERVER=${TFTP:-fsl00c}
FESA_EXPORT=/common/export/fesa
FESA_DATAMOUNT="$FESA_BASE/data"

# determine name of fec (name or nomenclature)
FEC_NAME=$(hostname)
FEC_NOMEN=$(hostnomen 2> /dev/null)
if [ $? -eq 0 ]; then
  log "Found nomenclature '$FEC_NOMEN' for frontend '$FEC_NAME'."
  FEC_NAME=$FEC_NOMEN
else
  log "No nomenclature found for frontend '$FEC_NAME'."
fi
log "Getting DUs from '$FESA_MOUNT/local/$FEC_NAME'."

FESA_DATAEXPORT=/common/fesadata/data/$FEC_NAME
FESA_DUFOLDER="$FESA_BASE/du"
FESA_CONFIGS="cmw.cfg fesa.cfg log.cfg messages.cfg messagesLab.cfg"

log "fesa nfsinit start"

# 64bit fesa in a ramdisk, 
# no additional libraries added, no environment config
# just /opt/fesa/data for persistence

[ -d "$FESA_MOUNT" ] || mkdir -p "$FESA_MOUNT"
[ -d "$FESA_DATAMOUNT" ] || mkdir -p "$FESA_DATAMOUNT"
[ -d "$FESA_DUFOLDER" ] || mkdir -p "$FESA_DUFOLDER"


# persistence data
if grep -q "$FESA_DATAMOUNT" /proc/mounts; then
  log_debug "fesa data already mounted"
else
  mount -t nfs -o rw,nolock,nfsvers=3 $FESA_SERVER:$FESA_DATAEXPORT $FESA_DATAMOUNT
  if [ $? -eq 0 ]; then
    log "mounted $FESA_SERVER:$FESA_DATAEXPORT $FESA_DATAMOUNT"
  else
    log_fatal "failed to mount $FESA_SERVER:$FESA_DATAEXPORT $FESA_DATAMOUNT"
  fi
fi

# mount nfs to copy deploy units
# could be unmounted after startup
if grep -q "$FESA_MOUNT" /proc/mounts; then
  log_debug "nfs already mounted"
else
  [ -d "$FESA_MOUNT" ] || mkdir -p "$FESA_MOUNT"
  mount -t nfs -o ro,nolock,nfsvers=3 $FESA_SERVER:$FESA_EXPORT $FESA_MOUNT
  if [ $? -eq 0 ]; then
    log "mounted $FESA_SERVER:$FESA_EXPORT $FESA_MOUNT"
  else
    log_fatal "failed to mount $FESA_SERVER:$FESA_EXPORT $FESA_MOUNT"
  fi
fi

log "install CMX library and tools into RAM disc"
CMX_DIR=$FESA_EXPORT/cmw-cmx
CMX_MOUNT=/cmx
[ ! -d $CMX_MOUNT ] && mkdir -p $CMX_MOUNT
mount -t nfs -o rw,nolock,nfsvers=3 $FESA_SERVER:$CMX_DIR $CMX_MOUNT

log "install CMX library and tools into RAM disc"
install -m 755 $CMX_MOUNT/lib/libcmw-cmx.a /usr/lib/
install -m 755 $CMX_MOUNT/lib/libcmw-cmx-cpp.a /usr/lib/
install -m 755 $CMX_MOUNT/lib/libcmw-cmx.so /usr/lib/
install -m 755 $CMX_MOUNT/lib/libcmw-cmx-cpp.so /usr/lib/

install -m 755 $CMX_MOUNT/tools/* /usr/bin/

# adjust mqueue sizes
/sbin/sysctl -w fs.mqueue.msg_max=200 1>/dev/null
/sbin/sysctl -w fs.mqueue.msgsize_max=32768 1>/dev/null


for DU_FOLDER in $FESA_MOUNT/local/$FEC_NAME/*; do
  if [ ! -d $DU_FOLDER ]; then
    continue  # skip non-dirs and nullglob
  fi
  DU=$(basename "$DU_FOLDER")
  # check for all files present
  for f in $DU $DU.instance $FESA_CONFIGS; do
    if [ ! -e $FESA_MOUNT/local/$FEC_NAME/$DU/$f ]; then
      log "ignoring $DU. not a deploy unit. missing $f"
      continue 2  # continue with next $DU_FOLDER
    fi
  done

  log "found DU $DU"

  # cleanup
  if [ -d $FESA_DUFOLDER/$DU ]; then
    rm -rf $FESA_DUFOLDER/$DU
  fi
  
  mkdir -p $FESA_DUFOLDER/$DU
  # install files
  for FILE in $DU_FOLDER/*; do
    cp $FILE $FESA_DUFOLDER/$DU
    #install -m 644 $FILE $FESA_DUFOLDER/$DU
  done
  
  START_SCRIPT="$FESA_DUFOLDER/$DU/startManually_${DU}_M.sh"
  chmod +x $START_SCRIPT

  # crude check if DU is using white rabbit timing
  # for some reason fesa DU requires special parameters
  # even if the information should be parsed by the DU itself
  if grep -q 'timingDomain value="FAIR"' $FESA_DUFOLDER/$DU/$DU.instance ; then
    FESA_WR='-usrArgs -WR'
  else
    FESA_WR=''
  fi

  # default delays of daemon are used:
  # process must run for 300 seconds
  # we will try to respawn it 5 times
  # we will wait 300 seconds

# Tidy up old coredumps
rm -rf /*.core

# Specifiy a location for coredumps (no continous naming to prevent running out of memory, because we are using a ramdisk)
echo "/latest_crash.core" > /proc/sys/kernel/core_pattern

# Alternative Way to keep a process alive:
# Attention!! Seems not to work any more on logout !
# (For unknown reason daemon does not survive a self-kill)
#(while true; do 
#    $FESA_DUFOLDER/$DU/startManually_DigitizerDU2_M.sh
#    sleep 1
#done) &

  DAEMON_OPTS="-n $DU -r -E daemon.err"
  if daemon $DAEMON_OPTS --running; then
    log "restarting DU $DU"
    daemon $DAEMON_OPTS --restart
  else
    log "starting DU $DU"
    daemon $DAEMON_OPTS $START_SCRIPT
  fi
done