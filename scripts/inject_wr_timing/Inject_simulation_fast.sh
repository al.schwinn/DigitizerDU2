#!/bin/sh
# SIS18 Ring
GID=300 

# Arguments: GroupID, EventID, SequenceID, ProcessID, ChainID, Flags
inject()
{
    id=0
    mask=0
    param=0
    
    #FormatID
    id=$((0x1<<60))
    mask=$((0xf<<60))
    
    #GroupID
    if [ "$#" -gt 0 ]
    then
      id=$(( (id) |  ($1<<48)  ))
      mask=$(( mask | (0xfff<<48) ))
    fi
    #EventID
    if [ "$#" -gt 1 ]
    then
      id=$(( (id) | ($2<<36)  ))
      mask=$(( mask | (0xfff<<36) ))
    fi
    #SequenceID
    if [ "$#" -gt 2 ]
    then
      id=$(( (id) | ($3<<20)  ))
      mask=$(( mask | (0xfff<<20) ))
    fi
    #BeamProcessID
    if [ "$#" -gt 3 ]
    then
      id=$(( (id) | ($4<<6)  ))
      mask=$(( mask | (0x3fff<<6) ))
    fi
    
    if [ "$#" -gt 4 ]
    then
      param=$(( (param) | ($5<<42)  ))
    fi
    
    #Flags Go Here
    if [ "$#" -gt 5 ]
    then
      id=$(( (id) | ($6<<32)  ))
    fi
    
    saft-ctl dummy -f inject $id $param 5000000
}

# Argumetns:
#  - $1: IO name
#
configure_io()
{
  echo ""

  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "configuring $1, make sure to restart FESA class afterwards (output conditions will be deleted)"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

  echo ""

  saft-io-ctl tr0 -n $1 -x

  saft-io-ctl tr0 -n $1 -o1 -d0

  saft-io-ctl tr0 -n $1 -u -c  0x1023000000000     0xfffffff000000000 2500000 0 1
  saft-io-ctl tr0 -n $1 -u -c  0x1023000000000     0xfffffff000000000 3500000 0 0

  saft-io-ctl tr0 -i
  saft-io-ctl tr0 -l
  echo ""x
}

# Argumetns:
#  - $1: IO name
#
configure_io_timing()
{
  echo ""

  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "configuring $1, make sure to restart FESA class afterwards (output conditions will be deleted)"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

  echo ""

  saft-io-ctl baseboard -n $1 -o1 -d0

  # TIME_SYNC
  saft-io-ctl baseboard -n $1 -u -c  0x107B000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x107B000000000     0xfffffff000000000 2500000 0 0

  # INJECT
  saft-io-ctl baseboard -n $1 -u -c  0x1023000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1023000000000     0xfffffff000000000 2500000 0 0

  # USER 1
  saft-io-ctl baseboard -n $1 -u -c  0x1118000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1118000000000     0xfffffff000000000 2500000 0 0

  # USER 2
  saft-io-ctl baseboard -n $1 -u -c  0x1119000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1119000000000     0xfffffff000000000 2500000 0 0

  # USER 3
  saft-io-ctl baseboard -n $1 -u -c  0x111A000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x111A000000000     0xfffffff000000000 2500000 0 0

  saft-io-ctl baseboard -i
  saft-io-ctl baseboard -l
  echo ""
}

show_config()
{
  echo ""
  saft-io-ctl tr0 -l
  echo ""
}

# Executes a sequence (takes ~1sec with current time settings)
# Argumetns:
#  - $1: sequnce number (should be a single digit)
#

execute_sequnce()
{
  echo "executing sequnce" $1 `date`

  # SEQ_START#257
  inject $GID 257 $1 1 0 
  usleep 10000

  # BP_START#256
  inject $GID 256 $1 1 0 
  usleep 10000
  
  # CMD_BEAM_INJECTION#283 - used instead of "bean in" flag
  inject $GID 283 $1 4
  usleep 1000
  
  # CMD_FG_START#513    Start function generator (0x8 = beam in)
  inject $GID 513 $1 4 0x8
  usleep 1000
  
  # BP_START#256
  inject $GID 256 $1 1 0 
  usleep 1000
  
  # CMD_CUSTOM_DIAG_1#286
  inject $GID 286 $1 4 0x8
  usleep 100

  # CMD_START_ENERGY_RAMP#285
  inject $GID 285 $1 4 0x8
  usleep 4900

  # CMD_CUSTOM_DIAG_2#287
  inject $GID 287 $1 4 0x8
  usleep 10000
  
  # CMD_CUSTOM_DIAG_3#288
  inject $GID 288 $1 4 0x8
  usleep 10000
  
  # CMD_GAP_START#258
  inject $GID 258 $1 4
  usleep 10000
  
  # CMD_BEAM_EXTRACTION#284
  inject $GID 284 $1 4
  usleep 43000
  
  echo "end of sequnce" $1 `date`
}

execute_command_only()
{
  usleep 100000
  inject $GID 255 $1 4
  echo "COMAND sent"
}

# This will be done by FESA during startup
# configure_io IOI


# For gnuradio only
# configure_io_timing IO1

while true
do
  #execute_command_only
  execute_sequnce 3
  
  # sometimes sleeps 5sec between sequences .. more realistic (required t reproduce prod. bugs from dal007 )
  #usleep 5000000
  
  execute_sequnce 7
  
  # sometimes sleeps 5sec between sequences .. more realistic (required t reproduce prod. bugs from dal007 )
 # usleep 5000000
  show_config
done

