#!/bin/sh
# SIS18 Ring
GID=300 

# Arguments: GroupID, EventID, SequenceID, ProcessID, ChainID, Flags
inject()
{
    id=0
    mask=0
    param=0
    
    #FormatID
    id=$((0x1<<60))
    mask=$((0xf<<60))
    
    #GroupID
    if [ "$#" -gt 0 ]
    then
      id=$(( (id) |  ($1<<48)  ))
      mask=$(( mask | (0xfff<<48) ))
    fi
    #EventID
    if [ "$#" -gt 1 ]
    then
      id=$(( (id) | ($2<<36)  ))
      mask=$(( mask | (0xfff<<36) ))
    fi
    #SequenceID
    if [ "$#" -gt 2 ]
    then
      id=$(( (id) | ($3<<20)  ))
      mask=$(( mask | (0xfff<<20) ))
    fi
    #BeamProcessID
    if [ "$#" -gt 3 ]
    then
      id=$(( (id) | ($4<<6)  ))
      mask=$(( mask | (0x3fff<<6) ))
    fi
    
    if [ "$#" -gt 4 ]
    then
      param=$(( (param) | ($5<<42)  ))
    fi
    
    #Flags Go Here
    if [ "$#" -gt 5 ]
    then
      id=$(( (id) | ($6<<32)  ))
    fi
    
    saft-ctl dummy -f inject $id $param 5000000
}

# Argumetns:
#  - $1: IO name
#
configure_io()
{
  echo ""

  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "configuring $1, make sure to restart FESA class afterwards (output conditions will be deleted)"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

  echo ""

  saft-io-ctl tr0 -n $1 -x

  saft-io-ctl tr0 -n $1 -o1 -d0

  saft-io-ctl tr0 -n $1 -u -c  0x1023000000000     0xfffffff000000000 2500000 0 1
  saft-io-ctl tr0 -n $1 -u -c  0x1023000000000     0xfffffff000000000 3500000 0 0

  saft-io-ctl tr0 -i
  saft-io-ctl tr0 -l
  echo ""x
}

# Argumetns:
#  - $1: IO name
#
configure_io_timing()
{
  echo ""

  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "configuring $1, make sure to restart FESA class afterwards (output conditions will be deleted)"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

  echo ""

  saft-io-ctl baseboard -n $1 -o1 -d0

  # TIME_SYNC
  saft-io-ctl baseboard -n $1 -u -c  0x107B000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x107B000000000     0xfffffff000000000 2500000 0 0

  # INJECT
  saft-io-ctl baseboard -n $1 -u -c  0x1023000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1023000000000     0xfffffff000000000 2500000 0 0

  # USER 1
  saft-io-ctl baseboard -n $1 -u -c  0x1118000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1118000000000     0xfffffff000000000 2500000 0 0

  # USER 2
  saft-io-ctl baseboard -n $1 -u -c  0x1119000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1119000000000     0xfffffff000000000 2500000 0 0

  # USER 3
  saft-io-ctl baseboard -n $1 -u -c  0x111A000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x111A000000000     0xfffffff000000000 2500000 0 0

  saft-io-ctl baseboard -i
  saft-io-ctl baseboard -l
  echo ""
}

show_config()
{
  echo ""
  saft-io-ctl tr0 -l
  echo ""
}

# Executes a sequence (takes ~1sec with current time settings)
# Argumetns:
#  - $1: sequnce number (should be a single digit)
#

execute_command_only()
{
  usleep 1000000
  inject $GID 255 $1 4
  echo "COMAND sent"
}

# For gnuradio only
# configure_io_timing IO1

while true
do
  execute_command_only
  show_config
done

