#!/bin/sh

# exit on any error
set -e

# Script to update the gateware and theinstalled Digitizer version

function help {
    echo ""
    echo "Usage: updateTimingGatewareAndDigitizer.sh <FEC-Name>"
    echo ""
    echo "This script will udate the gateware and the Digitizer binary of a FEC to the specified version"
    echo ""
    exit 1;
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

if [ $# -eq 0 ]
  then
    echo "Error: No arguments supplied. First and only argument has to be FEC-Name."
    help
    exit 1
fi
if [ $# -ne 1 ]
  then
    echo "Error: Wrong number arguments supplied. First and only argument has to be FEC-Name"
    help
    exit 1
fi

# Ask the user which fiormware to use
echo -e "There are two different Timing Cards available for the supermicro:"
echo -e "\t 1. Pexaria (3IO Ports)"
echo -e "\t 2. PexP    (5IO Ports)"
echo -e ""
echo -e "You might want to check via 'eb-info dev/wbm0'"
read -p "For which card do you want to flash gateware? (press '1' or '2') " answer

if [[ "$answer" == "1" ]]; then
    echo "Downloading firmware for Pexaria ..."
    NEW_TIMING_FIRMWARE=falloutv6_1_2-pexarria5.rpd
elif [[ "$answer" == "2" ]]; then
    echo "Downloading firmware for Pexp ..."
    NEW_TIMING_FIRMWARE=falloutv6_1_2-pexp.rpd
else
    echo "Invalid input. Exiting"
    exit 1
fi

FECNAME=$1

NFS_INIT_DIR=/common/export/nfsinit/${FECNAME}
NFS_INIT_DIR_FEC=/opt/nfsinit/${FECNAME}
NEW_TIMING_VERSION=fallout-v6.1.2
NEW_TIMING_FIRMWARE_URL=https://github.com/GSI-CS-CO/bel_projects/releases/download/${NEW_TIMING_VERSION}/${NEW_TIMING_FIRMWARE}

# Still required for el7 images, can be droped if we move to yocto
rm -fv ${NFS_INIT_DIR}/20_timing*
ln -fvs ../global/timing-rte-tg-${NEW_TIMING_VERSION} ${NFS_INIT_DIR}/20_timing_rte

# Download firmware
cd ${NFS_INIT_DIR}
wget ${NEW_TIMING_FIRMWARE_URL}

echo "Updating Firmware"
ssh root@$FECNAME "
killall daemon;
rm -rf /opt/fesa/du/*;
killall daemon;
killall dbus-daemon;
killall socat;
killall DigitizerDU2;
killall saftd;
echo 'Getting pexaria MAC: ';
eb-mon dev/wbm0 -m;
echo 'done';
sleep 1;
cd ${NFS_INIT_DIR_FEC};
echo 'flashing new firmware';
eb-flash dev/wbm0 ${NEW_TIMING_FIRMWARE};
echo 'done';
sleep 1;
echo 'resetting fpga';
eb-reset dev/wbm0 fpgareset;
echo 'done';
sleep 1;
echo 'rebooting FEC';
/sbin/reboot;
echo 'done';"

# cleanup downloaded timing firmware
cd ${NFS_INIT_DIR}
rm ${NEW_TIMING_FIRMWARE}

# Wait until reboot finished
echo 'waiting till reboot finished ...'
STARTTIME=$(date +%s)
TIMEOUT=120
until nc -vzw 2 $FECNAME 22; do
NOW=$(date +%s)
if [ $(($NOW - $STARTTIME)) -gt $TIMEOUT ]
then
        echo "Timeout after $TIMEOUT seconds ----- Failed to wakeup $HOST -------------"
        exit 1
fi

sleep 2;
done

echo "$FECNAME is up now"
#sleep 3
#echo 'resetting usb and rebooting daemon'
#ssh root@$FECNAME "
#killall daemon;
#/opt/fesa/nfs/global/scripts/reset.usb.sh;"

exit 0
