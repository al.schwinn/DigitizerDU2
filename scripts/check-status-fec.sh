#!/bin/bash

# exit on any error
#set -e

function help {
    echo ""
    echo "Usage: reads the status of a fec <FEC_NAME>"
    echo ""
    echo "This script will read the status property of a FEC and return 0 if the status is ok"
    echo ""
    echo "-v verbose .. will as well print the uptime"
    echo ""
    exit 1;
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

if [ $# -gt 2 ]
  then
    echo "Error: Wrong number of arguments supplied."
    help
    exit 1
fi
if [ $# -lt 1 ]
  then
    echo "Error: Wrong number of arguments supplied."
    help
    exit 1
fi


FEC=$1
VERBOSE=FALSE
DEX=pdex

if [ ! -z "$2" ]; then
        VERBOSE=TRUE
fi

# Use idex for int systems
if [ "$FEC" = "fel0002" ] || [ "$FEC" = "fel0038" ]; then
        DEX=idex
fi

RESULT=$($DEX -a $FEC)

#echo "RESULT $RESULT"

# Expression that matches Digitizer Nomen prefixes
SEARCH_STRINGS="GSCD|YRCD|GECD|GUCD|ZZCD|DigitizerIntDevice"

# Use grep to search for the first occurrence of the regular expression
match=$(echo $RESULT | tr ' ' '\n' | grep -E $SEARCH_STRINGS | grep -v global)

if [[ -n "$match" ]]; then
  NOMEN="$match"
else
  echo "NOMEN not found error for FEC: $FEC" >&2
  exit 1
fi

#echo "NOMEN FOUND: $NOMEN"

STATUS=$($DEX -a $NOMEN Status | grep '| status' | awk '{print $9}')

if [ "$VERBOSE" = "TRUE" ]; then

  UPTIME=$($DEX -a $NOMEN SystemMonitor | grep '|-- uptime' | awk '{print $4}')

  echo -e "Status of $FEC ('$NOMEN'):\t$STATUS\tuptime: $UPTIME"
fi

if [ "$STATUS" = "OK" ]; then
  exit 0
else
  exit 1
fi