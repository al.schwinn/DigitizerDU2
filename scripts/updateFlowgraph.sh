#!/bin/sh

function help {
    echo ""
    echo "Usage: updateFlowgraph <FEC-Name> <Flowgraph>"
    echo ""
    echo "This script will upload the flowgraph to the specified FEC and restart the FESA class."
    echo "If no flowpgrap is passed, all flowgraphs inside the src/test/<FEC-Name> folder are uploaded."
    echo "For the FESA class restart, the root pw for the FEC is required"
    echo "Note that the name of the passed flowgraph has to match the one defined in the instance-file !"
    echo ""
    exit 1;
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

if [ $# -eq 0 ]
  then
    echo "Error: No arguments supplied. Check the help:
    help
fi

if [ $# -gt 2 ]
  then
    echo "Error: Wrong number of arguments supplied. Check the help:
    help
fi

SCRIPT=$(readlink -f "$0")
PROJECT_PATH=$(dirname "$(dirname "$SCRIPT")")    

FEC=$1
FEC_FOLDER=/common/export/fesa/local/$FEC/DigitizerDU2/

# Set these variables first
hostname=$1

if [ $# -eq 1 ]
  then
    echo "... removing persistancy file"
    rm -v /common/fesadata/data/$FEC/DigitizerDU2.${FEC}DigitizerClass2PersistentData.xml

    cp -v ${PROJECT_PATH}/src/test/${FEC}/*.grc $FEC_FOLDER
    echo "... copied all available flowgraphs to FEC"
  else
    cp -v $2 $FEC_FOLDER
    echo "... copied flowgraph to FEC"
    
fi

echo "Please give FEC root password to restart Digitizer FESA class on FEC:"
soft_reset_fec $FEC
