#!/bin/sh

# exit on any error
#set -e

function help {
    echo -e ""
    echo -e "Usage: apply-to-all-digitizer-fecs.sh"
    echo -e ""
    echo -e "This script will apply a specific command to all Digitizer FEC's "
    echo -e ""
    echo -e "\t--help \t\tShow this help text"
    echo -e "\t--check \tRead the status property of all digitizer FEC and show if they are ok."
    echo -e "\t--reset \tkill and restart all FESA applications on all digitizer FECs"
    echo -e "\t--reboot \treboot all digitizer FECs"
    echo -e "\t--release \tRelease the Digitizer software to all digitizer FECs. Append the version as argument."
    echo -e "\t--ping \t\tPing all Digitizer FECs with a given timeout."
}

options=$(getopt -o h --long help,ping,check,reset,reboot,release:, -- "$@")
[ $? -eq 0 ] || { 
    echo "Incorrect options provided"
    help
    exit 1
}

eval set -- "$options"
while true; do
    case "$1" in
    --help | -h)
        help
        exit 1
        ;;
    --check)
        MODE=CHECK
        break
        ;;
    --reset)
        MODE=SOFT_RESET
        break
        ;;
    --reboot)
        MODE=REBOOT
        break
        ;;
     --ping)
        MODE=PING
        break
        ;;
    --release)
        MODE=RELEASE
        shift; # Version argument
        VERSION=$1
        break
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

SCRIPT=$(readlink -f "$0")
PROJECT_PATH="$(dirname "$SCRIPT")"     # path where this script is located in
FEC_FOLDERS=/home/bel/schwinn/lnx/git/DigitizerDU2/src/test/*

# dal004, fel0013 --> development systems
# dal015 --> HF Scope which is not installed yet
# fel0024 --> ps5000 not yet supported
IGNORE_FECS="dal004 fel0013 dal015 fel0024"

declare -a FECS

for folder in $FEC_FOLDERS
do
    FEC=$(basename $folder)
    IGNORED=FALSE
    for IGNORE_FEC in $IGNORE_FECS
    do
      if [ "$FEC" = "$IGNORE_FEC" ]; then
        IGNORED=TRUE
      fi
    done
    if [ "$IGNORED" = "TRUE" ]; then
        #echo "ignoring  FEC $FEC"
        continue
    fi
    FECS+=($FEC)
done

if [ "$MODE" = "SOFT_RESET" ] || [ "$MODE" = "REBOOT" ]; then
     echo tesz1
    #if [ -z "$SSH_AUTH_SOCK" ] ; then
        echo "Starting ssh-agent and loading ssh-keys"
        eval `ssh-agent -s`
        ssh-add
    #fi
fi

if [ "$MODE" = "PING" ]; then
   $PROJECT_PATH/ping-fecs.sh ${FECS[@]}
   exit 0
fi

for FEC in "${FECS[@]}"
do
    if   [ "$MODE" = "CHECK" ]; then
        $PROJECT_PATH/check-status-fec.sh $FEC -v
    elif [ "$MODE" = "RELEASE" ]; then
        echo "releasing FEC: $FEC"
        $PROJECT_PATH/releaseDigitizer.sh $FEC $VERSION
    elif [ "$MODE" = "SOFT_RESET" ]; then
        echo "soft resetting FEC: $FEC"
        soft_reset_fec $FEC
    elif [ "$MODE" = "REBOOT" ]; then
        echo "rebooting $FEC"
        ssh root@$FEC "
        echo ' - resetting fpga';
        eb-reset dev/wbm0 fpgareset;
        sleep 1;
        echo ' - rebooting FEC';
        /sbin/reboot;
        echo ' - reboot done';"
    else
        echo "Unknown mode"
        help
        exit 1
    fi
done

echo "apply to all digitizers finished"
exit 0