#!/bin/sh

# exit on any error
set -e

function help {
    echo ""
    echo "Usage: ping-fecs -t <timeout_sec> <FEC_NAME_1> <FEC_NAME_1> ... <FEC_NAME_N>"
    echo ""
    echo "This script will ping a list of FECs, until they all are pinged sucessfully or a timeout was hit (default 60sec)"
    echo "note that 'nc' will be used for pinging, not the 'ping' command"
    echo ""
    exit 1;
}

function generate_doc_tagged {
    echo "<toolinfo>"
    echo "<name>ping-fecs</name>"
    echo "<topic>Maintenance</topic>"
    echo "<description>will ping a list of FECs, until they all are pinged sucessfully or a timeout was hit (default 60sec).</description>"
    echo "<usage>ping-fecs -t <timeout_sec> <FEC_NAME_1> <FEC_NAME_1> ... <FEC_NAME_N></usage>"
    echo "<author>Alexander Schwinn</author>"
    echo "<tags>ping,fec</tags>"
    echo "<version></version>"
    echo "<documentation></documentation>"
    echo "<environment>int/pro/dev</environment>"
    echo "<requires></requires>"
    echo "<autodocversion>bash</autodocversion>"
    echo "</toolinfo>"
    exit 0
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

if [[ "$1" == "--generate_doc_tagged" ]];
then
    generate_doc_tagged
fi

TIMEOUT_SECONDS=60
if [[ "$1" == "-t" ]];
then
    shift
    TIMEOUT_SECONDS=$1
    shift
fi

if [ $# -eq 0 ]
  then
    echo "Error: No arguments supplied."
    help
    exit 1
fi

# Dont reset starttime after each attempt
# E.g. after rebooting sever FECs, we dont want to wait the timeout multiple times
STARTTIME=$(date +%s)

for FECNAME in "$@"
do
    echo "pinging $FECNAME (Timeout $TIMEOUT_SECONDS seconds)"

    TIMEOUT=false
    until nc -zw 2 $FECNAME 22
    do
        NOW=$(date +%s)
        if [ $(($NOW - $STARTTIME)) -gt $TIMEOUT_SECONDS ]
        then
            TIMEOUT=true
            break
        fi
        sleep 1;
    done

    if [ "$TIMEOUT" = true ]; then
        echo "$FECNAME - timeout after waiting $TIMEOUT_SECONDS seconds ----- Failed to wake up $FECNAME !! -------------"
    else
        echo "$FECNAME - is up"
    fi

done

exit 0