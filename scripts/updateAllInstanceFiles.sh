#!/bin/sh

# This script generates a diff from a modified .instance file, and applies the diff to all other instance files of the DU

# Note that there is as well the tool "fesa-instance" which can be used to modify multiple instances files at once,
# Though "fesa-instance" only works at device level and has a rather complex API. See: https://www-acc.gsi.de/wiki/FESA/FESA-Update-InstanceFiles

if [ $# -ne 1 ]
  then
    echo "Error: Wrong number arguments supplied. Required arguments: [FEC]"
    exit 1
fi

GOOD_FEC_PATH="/home/bel/schwinn/lnx/git/DigitizerDU2/src/test/$1"
DIFF_FILE="$GOOD_FEC_PATH/temp.diff"
echo $GOOD_FEC_PATH
cd $GOOD_FEC_PATH

# This uses 3 matching lines for diff matching
# git diff DeviceData_DigitizerDU2.instance > $DIFF_FILE

# This uses just a single line .. better if as well other lines nearby are different
git diff --unified=1 --patch DeviceData_DigitizerDU2.instance > $DIFF_FILE

FEC_FOLDERS=/home/bel/schwinn/lnx/git/DigitizerDU2/src/test/*

for folder in $FEC_FOLDERS
do
    if [[ $folder != $GOOD_FEC_PATH ]]; then
        echo "Processing $folder"
        cd $folder
        patch DeviceData_DigitizerDU2.instance < $DIFF_FILE
        rm *.orig
    fi
done

rm $DIFF_FILE