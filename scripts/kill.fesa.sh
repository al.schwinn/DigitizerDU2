
SERVER=$(hostname)
ps -A | grep ${SERVER} | awk '{print $1}' | xargs kill -9 $1 &> /dev/null
