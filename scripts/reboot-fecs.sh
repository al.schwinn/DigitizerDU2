#!/bin/sh

function help {
    echo ""
    echo "Usage: reboot-fecs <FEC_NAME_1> <FEC_NAME_1> ... <FEC_NAME_N>"
    echo ""
    echo "This script will do a hard reboot and fpga reset on all passed FEC's while only requesting the root password once"
    echo "Prerequirement: FEC needs to use the '../global/cscofe' symlink in /common/export/nfsinit/<FEC_NAME> "
    echo ""
    exit 1;
}

function generate_doc_tagged {
    echo "<toolinfo>"
    echo "<name>reboot-fecs</name>"
    echo "<topic>Maintenance</topic>"
    echo "<description>This script will do a hard reboot and fpga reset on all passed FEC's while only requesting the root password once</description>"
    echo "<usage>reboot-fecs <FEC_NAME_1> <FEC_NAME_1> ... <FEC_NAME_N></usage>"
    echo "<author>Alexander Schwinn</author>"
    echo "<tags>reboot,reset,fec</tags>"
    echo "<version></version>"
    echo "<documentation></documentation>"
    echo "<environment>int/pro/dev</environment>"
    echo "<requires></requires>"
    echo "<autodocversion>bash</autodocversion>"
    echo "</toolinfo>"
    exit 0
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

if [ $# -eq 0 ]
  then
    echo "Error: No arguments supplied."
    help
    exit 1
fi

# Start ssh-agent and load keys, if not already started
if [ -z "$SSH_AUTH_SOCK" ] ; then
  eval `ssh-agent -s`
  ssh-add
fi

for FECNAME in "$@"
do
    echo "rebooting $FECNAME"

    ssh root@$FECNAME "
    echo ' - resetting fpga';
    eb-reset dev/wbm0 fpgareset;
    sleep 1;
    echo ' - rebooting FEC';
    /sbin/reboot;
    echo ' - reboot done';"

done

exit 0
