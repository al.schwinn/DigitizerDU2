#!/bin/sh

# exit on any error
set -e

# Script to generate all needed symlinks / folders, etc. for a new digitizer FEC

if [ $# -ne 1 ]
  then
    echo "Error: Wrong number arguments supplied. Required arguments: [FEC-Name]"
    exit 1
fi


# Set these variables first
FEC_TYPE=supermicro
MAJOR_VERSION=5
MINOR_VERSION=0
BUGFIX_VERSION=0
VERSION="${MAJOR_VERSION}.${MINOR_VERSION}.${BUGFIX_VERSION}"
HOST=$1
NFS_INIT_DIR=/common/export/nfsinit/${HOST}
mgm_page=http://${HOST}i.acc.gsi.de

echo ""
echo "Please set the password for the management port default account (ADMIN) to the same PW the other Digitizer FEC's do use!"
echo "Will open the server management page in a webbrowser for you ..."
exo-open $mgm_page 1> /dev/null 2>&1
echo ""
echo "To do so, go to: Configuration --> Users --> ADMIN --> modify User."
echo ""
read -n 1 -s -r -p "Done? Press any key to continue"
echo ""
echo "Will specify some Ramdisk for the FEC and create required symlinks, so it is able to boot at all .. "
echo ""
pxe-config -r ${HOST}
pxe-config -c ${HOST} scuxl.el7

echo "creating nfsinit directoy and symlinks" 
mkdir -pv ${NFS_INIT_DIR}
ln -fvs ../global/digitizer-${MAJOR_VERSION} ${NFS_INIT_DIR}/01_digitizer-${MAJOR_VERSION}
ln -fvs ../global/digitizer_optimization_${FEC_TYPE} ${NFS_INIT_DIR}/02_digitizer_optimization_${FEC_TYPE}
ln -fvs ../global/digitizer_fesa_environment ${NFS_INIT_DIR}/50_fesa
ln -fvs ../global/timing-rte-tg-fallout-v6.1.2 ${NFS_INIT_DIR}/20_timing-rte
ln -fvs ../global/cscofe ${NFS_INIT_DIR}/60_cscofe

# Required to have a fesadata folder. Otherwise 50_fesa will fail
mkdir -vp /common/fesadata/data/${HOST}

echo ""
echo "In order to see the boot sequence (and to change bios settings), please open a boot console."
echo "Remote Control --> IVKM/HTML5 --> IVKM/HTML5. Ignore any firefox warning regarding video streaming."
echo ""
read -n 1 -s -r -p "Done? Press any key to continue"
echo ""
echo "Now please boot the system. Power Control --> Set Power On"
echo ""
read -n 1 -s -r -p "Done? Press any key to continue"
echo ""
echo "Now quickly press the 'del' key to enter the bios setup."
echo ""
read -n 1 -s -r -p "Done? Press any key to continue"
echo ""
echo "In order to prevent the need to reboot all systems by hand after a power outage, we need to change a bios setting:"
echo "Advanced --> Boot Feature --> Restore on AC Power Loss --> Last State."
echo ""
echo "Save & Exit --> Save Changes and Reset"
read -n 1 -s -r -p "Done? Press any key to continue"
echo ""

echo "waiting till boot finished ..."
STARTTIME=$(date +%s)
TIMEOUT=120
until nc -vzw 2 $HOST 22; do
NOW=$(date +%s)
if [ $(($NOW - $STARTTIME)) -gt $TIMEOUT ];then
        echo "Timeout after ${TIMEOUT} seconds ----- Failed to wakeup ${HOST} -------------"
fi
sleep 2;
done

# Update Timiming Gateware
SCRIPT=$(readlink -f "$0")
SCRIPT_FOLDER="$(dirname "$SCRIPT")"     # path where this script is located in
$SCRIPT_FOLDER/updateTimingGateware.sh $HOST

echo ""
echo "Log in to the new server in order to see if timing link is UP and in SYNC (sss $HOST)"
echo ""
read -n 1 -s -r -p "Is timing working properly ? Press any key to continue"
echo ""

echo "Now you will need to write a mail to csco-tg@gsi.de in order to register the pexaria. The MAC can be found in the console output above"
echo "Required Information: !FEC-Name, Room, Serverrack, Responsible, Pexaria MAC-Address"
echo "Note that you only are allowed to connect the FEC to pro-timing if it is registered !!!"
echo ""
read -n 1 -s -r -p "Email sent? Press any key to continue"
echo ""

echo "creating fesa DU folders and symlinks" 
FESA_DIR=/common/export/fesa/local/$HOST
mkdir -vp ${FESA_DIR}/DigitizerDU2-${VERSION}
if [ -L ${FESA_DIR}/DigitizerDU2-d ]; then
  unlink ${FESA_DIR}/DigitizerDU2-d
fi
if [ -L ${FESA_DIR}/DigitizerDU2 ]; then
  unlink ${FESA_DIR}/DigitizerDU2
fi
ln -s DigitizerDU2-${VERSION} ${FESA_DIR}/DigitizerDU2-d
ln -s DigitizerDU2-d ${FESA_DIR}/DigitizerDU2

echo "Congratulation, you sucessfully added a new Digitizer FEC !!"
