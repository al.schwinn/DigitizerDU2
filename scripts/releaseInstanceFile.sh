#!/bin/sh

if [ $# -ne 1 ]
  then
    echo "Error: No arguments supplied. Call releaseDigitizer <FEC-Name>"
    exit 1
fi

FEC=$1
SCRIPT=$(readlink -f "$0")
PROJECT_PATH=$(dirname "$(dirname "$SCRIPT")")     # path where this script is located in

FEC_FOLDER=/common/export/fesa/local/$FEC/DigitizerDU2

## instance file ##
cp -v $PROJECT_PATH/src/test/$FEC/DeviceData_DigitizerDU2.instance $FEC_FOLDER/DigitizerDU2.instance

echo ""
echo "release complete"
