#!/bin/sh

# Use -i to trigger instance file upload to DB

function help {
    echo -e ""
    echo -e "Usage: releaseDigitizer <FEC-Name> <DigitizerClass-Version> [-e][-r]"
    echo -e ""
    echo -e "\t -e \t Export the instance file to the database"
    echo -e "\t -r \t Soft reset FEC after release"
    echo -e ""
    exit 1;
}

if [ $# -lt 2 ]
  then
    echo "Error: Wrong number of arguments supplied."
    help
fi

if [ $# -gt 4 ]
  then
    echo "Error: Wrong number of arguments supplied. "
    help
fi

SCRIPT=$(readlink -f "$0")
PROJECT_PATH=$(dirname "$(dirname "$SCRIPT")")     # path where this script is located in

FEC=$1
VERSION=$2
EXPORT_TO_DB=FALSE
SOFT_RESET_FEC=FALSE
shift 2

while getopts er flag
do
    case "${flag}" in
        e ) EXPORT_TO_DB=TRUE;;
        r ) SOFT_RESET_FEC=TRUE;;
    esac
done

FEC_FOLDER=/common/export/fesa/local/$FEC/DigitizerDU2-$VERSION
FESA_VERSION=7.0.0
FESA_ENVIRONMENT

if [[ "${FEC}" == "fel0038" || "${FEC}" == "fel0002" ]]; then
    FESA_ENVIRONMENT=INT
elif [[ "${FEC}" == "dal004" || "${FEC}" == "fel0013" ]]; then
    FESA_ENVIRONMENT=DEV
else
    FESA_ENVIRONMENT=PRO
fi

mkdir -p $FEC_FOLDER

## binray ##
cp -v $PROJECT_PATH/build/bin/x86_64/DigitizerDU2_M $FEC_FOLDER/DigitizerDU2

## fesa config files ##
cp -v $PROJECT_PATH/scripts/fesa.cfg $FEC_FOLDER
cp -v $PROJECT_PATH/scripts/log.cfg $FEC_FOLDER
cp -v /common/export/fesa/global/etc/fesa/$FESA_VERSION/cmw.cfg $FEC_FOLDER
cp -v /common/export/fesa/global/etc/fesa/$FESA_VERSION/messages.cfg $FEC_FOLDER
cp -v /common/export/fesa/global/etc/fesa/$FESA_VERSION/messagesLab.cfg $FEC_FOLDER

## usb reset helper ##
cp -v $PROJECT_PATH/scripts/nfsinit/reset.usb /common/export/fesa/local/$FEC/
chmod +x /common/export/fesa/local/$FEC/reset.usb

## instance file ##
cp -v $PROJECT_PATH/src/test/$FEC/DeviceData_DigitizerDU2.instance $FEC_FOLDER/DigitizerDU2.instance

## fix & release start scripts ##
FESA_ENVIRONMENT_LOWER_CASE=${FESA_ENVIRONMENT,,}
sed -i "s/cmwdev/cmw${FESA_ENVIRONMENT_LOWER_CASE}/g" $PROJECT_PATH/src/test/$FEC/startManually_DigitizerDU2_M.sh
sed -i "s/cmwpro/cmw${FESA_ENVIRONMENT_LOWER_CASE}/g" $PROJECT_PATH/src/test/$FEC/startManually_DigitizerDU2_M.sh

## enable codedumps
sed -i '123 a ulimit -c unlimited' $PROJECT_PATH/src/test/$FEC/startManually_DigitizerDU2_M.sh

cp -v $PROJECT_PATH/src/test/$FEC/startManually_DigitizerDU2_M.sh $FEC_FOLDER

## helper scripts ##
cp -v $PROJECT_PATH/scripts/kill.fesa.sh $FEC_FOLDER

## grc files ##
cp -v $PROJECT_PATH/src/test/$FEC/*.grc $FEC_FOLDER 2>/dev/null
#cp -v $PROJECT_PATH/grc/*.grc $FEC_FOLDER

if [ -f ${FEC_FOLDER}/${FEC}.grc ]; then
  if [ -L ${FEC_FOLDER}/flowgraph.grc ]; then
    unlink ${FEC_FOLDER}/flowgraph.grc
  fi
  ln -s ${FEC}.grc ${FEC_FOLDER}/flowgraph.grc
fi

## Timing Simulation ##
cp -v $PROJECT_PATH/scripts/inject_wr_timing/Inject_simulation*.sh $FEC_FOLDER
chmod +x $PROJECT_PATH/scripts/inject_wr_timing/Inject_simulation*.sh

# Update/Overwrite daemon nfsinit link with our own version
cd /common/export/nfsinit/${FEC}
if [ -L 50_fesa ]; then
    rm 50_fesa
fi
if [ -L 50_fesa_int ]; then
    rm 50_fesa_int
fi
ln -fvs ../global/digitizer_fesa_environment 50_fesa

# Update/Overwrite digitizer link
if [ -L 01_digitizer-4 ]; then
    rm 01_digitizer-4
fi
ln -fvs ../global/digitizer-5 01_digitizer-5

# Make sure this FEC is monitored by https://monitoring.fec.acc.gsi.de/icinga/
touch monitored_fec

# Re-link the DeployUnit folders to newly installed folder
cd /common/export/fesa/local/${FEC}
if [ -L DigitizerDU2 ]; then
    unlink DigitizerDU2
fi
if [ -L DigitizerDU2-d ]; then
    unlink DigitizerDU2-d
fi

# If there is a folder "DigitizerDU2-d", we backup it
if [ -d DigitizerDU2-d ]; then
    mv DigitizerDU2-d DigitizerDU2-d.backup
fi

ln -s DigitizerDU2-$VERSION DigitizerDU2
ln -s DigitizerDU2-$VERSION DigitizerDU2-d

# Generate new FEX zip file and copy it to /common/usr/fesa/htdocs/fex
mkdir $PROJECT_PATH/src/test/$FEC/DigitizerClass2.$VERSION
cp $PROJECT_PATH/src/test/$FEC/DeviceData_DigitizerDU2.instance $PROJECT_PATH/src/test/$FEC/DigitizerClass2.${VERSION}/$FEC.instance
cp $PROJECT_PATH/../DigitizerClass2/src/DigitizerClass2.design $PROJECT_PATH/src/test/$FEC/DigitizerClass2.${VERSION}/DigitizerClass2.${VERSION}.design
# 'ntgmNetworkNameneeds' to be in line 3 !
echo -e "\n\ntgmNetworkName=" > $PROJECT_PATH/src/test/$FEC/server.properties
cd $PROJECT_PATH/src/test/$FEC/
zip -r DigitizerDU2_$FEC.zip DigitizerClass2.${VERSION} server.properties
rm -r $PROJECT_PATH/src/test/$FEC/DigitizerClass2.${VERSION}
rm $PROJECT_PATH/src/test/$FEC/server.properties
cp -v $PROJECT_PATH/src/test/$FEC/DigitizerDU2_$FEC.zip /common/usr/fesa/htdocs/fex

## Only for development. For production, libraries and their dependencies should be released with:              ##
## gr-digitizers/createTarball.sh / gr-digitizers/createTarball_dependencies.sh / gr-flowgraph/createTarball.sh ##
#rm -vrf $FEC_FOLDER/libgnuradio-*
#cp -v /common/usr/cscofe/opt/gr-flowgraph/${VERSION}/lib64/libgnuradio-flowgraph-*.so.0.0.0 $FEC_FOLDER
#cp -v /common/usr/cscofe/opt/gr-digitizer/${VERSION}/lib64/libgnuradio-digitizers-*.so.0.0.0 $FEC_FOLDER

if [[ "${EXPORT_TO_DB}" == "TRUE" ]]; then
    echo "Exporting the Instance File to the Fesa DB (so new Properties can be used by JAPC)"
    INSTANCE_FILE=${PROJECT_PATH}/src/test/${FEC}/DeviceData_DigitizerDU2.instance
    patch ${INSTANCE_FILE} < $PROJECT_PATH/scripts/remove_command.diff
    echo "Exporting Instance File to ${FESA_ENVIRONMENT} DB"
    fesa3 -e all ${FESA_ENVIRONMENT} ${INSTANCE_FILE}
    git checkout -- DeviceData_DigitizerDU2.instance
    # some cleanup
    rm -f *.orig
    rm -f *.rej
    rm -f *.backup
fi
    
if [[ "${SOFT_RESET_FEC}" == "TRUE" ]]; then
    soft_reset_fec ${FEC}
fi

echo ""
echo "release complete"
